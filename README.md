# vale ouro :)

## Requirements:
-   [Docker](https://www.docker.com/)
-   [docker-compose](https://docs.docker.com/compose/install/)

## Steps:
1. Build the app
> docker-compose build

2. Run the app
> docker-compose up

3. Create the vendor path
> make vendor
